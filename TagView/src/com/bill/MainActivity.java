package com.bill;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.bill.page1.PageOneTagView;
import com.bill.page1.obj.PageObject;

public class MainActivity extends Activity {
	
	private Datapter datapter=new Datapter();
	private ArrayList<PageObject> list=new ArrayList<PageObject>();//标签列表
	private ListView lv_tag;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);		
		initData();
		initView();
	}
	
	
	/**
	 * 假设这边是从服务获取到的标签列表
	 */
	private void initData(){
		for(int i=0;i<10;i++){
			PageObject obj=new PageObject();
			obj.tagName="标签"+i;
			list.add(obj);
		}
	}
	
	private void initView(){
		lv_tag=(ListView)findViewById(R.id.lv_list);
		lv_tag.setAdapter(datapter);
	}
	
	

	class Datapter extends BaseAdapter {

		@Override
		public int getCount() {
			return 23;
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			viewHolder viewh;
			if (convertView == null) {
				viewh = new viewHolder();
				convertView = LayoutInflater.from(MainActivity.this).inflate(
						R.layout.list_item, parent, false);
				viewh.tag = (PageOneTagView) convertView.findViewById(R.id.page_tag);
				convertView.setTag(viewh);
			} else {
				viewh = (viewHolder) convertView.getTag();
			}
			/*
			 * 添加标签,并显示4个标签
			 */
			viewh.tag.setTagList(list, 4);

			return convertView;
		}

	}

	class viewHolder {
		public PageOneTagView tag;
	}
}
