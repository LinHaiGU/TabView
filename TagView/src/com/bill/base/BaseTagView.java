package com.bill.base;

import java.util.List;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bill.R;


/**
 * 标签，继承此抽象类
 * @author LinHai Gu
 *
 * @param <T>
 */
public abstract class BaseTagView<T> extends LinearLayout implements SelectTag<T>{

	private Context mContext;


	public BaseTagView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mContext = context;
		initView();
	}

	public BaseTagView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		initView();
	}

	public BaseTagView(Context context) {
		super(context);
		mContext = context;
		initView();

	}

	private void initView() {

		setOrientation(LinearLayout.HORIZONTAL);
	}

	

	/**
	 * 添加标签，可配
	 * @param datas 数据源
	 * @param num 显示的标签数
	 */
	public void addTag(List<T> datas,int num) {
		removeAllViews();
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		params.setMargins(0, 0, 10, 0);
		if (datas.size() > num) {
			for (int i = 0; i < num; i++) {
				T data = datas.get(i);
				TextView textView = (TextView) LayoutInflater.from(mContext)
						.inflate(R.layout.tagtext_view, null);
				textView.setText(getTag(data));
				textView.setLayoutParams(params);
				addView(textView);
			}
		} else {
			for (int i = 0; i < datas.size(); i++) {
				T data = datas.get(i);
				TextView textView = (TextView) LayoutInflater.from(mContext)
						.inflate(R.layout.tagtext_view, null);
				textView.setText(getTag(data));
				textView.setLayoutParams(params);
				addView(textView);

			}
		}

	}

}
