package com.bill.base;


/**
 * 
 * @author LinHai Gu
 *
 * @param <T>
 */
public interface SelectTag<T> {

	/**
	 * 获取标签内容
	 * @param data
	 * @return
	 */
	public abstract String getTag(T data);
	
}
