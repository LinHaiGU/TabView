package com.bill.page1;

import java.util.ArrayList;

import android.content.Context;
import android.util.AttributeSet;

import com.bill.base.BaseTagView;
import com.bill.page1.obj.PageObject;

public class PageOneTagView extends BaseTagView<PageObject>{

	public PageOneTagView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public PageOneTagView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public PageOneTagView(Context context) {
		super(context);
	}
	
	public void setTagList(ArrayList<PageObject> list,int num){
		addTag(list,num);
	}

	@Override
	public String getTag(PageObject data) {
		return data.tagName;
	}

}
