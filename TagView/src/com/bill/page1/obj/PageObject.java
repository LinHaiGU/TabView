package com.bill.page1.obj;

import java.io.Serializable;


/**
 * 假设这是某页面请求到的标签数据
 * @author LinHai Gu
 *
 */
public class PageObject implements Serializable{
	private static final long serialVersionUID = 100230230204204021L;
	public String tagName;
}
